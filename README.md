# WORDPRESS

## Purpose

Project to deploy the Wordpress application using Helm with remote chart from official repository.

## Values

This project uses the default values, if you want to change the values you may do the following.

1. Create the file `values.yaml` with the values you wish to change
2. Add the following to the file `helm.tf` under helm_release.wordpress-helm:
```yaml
  values = [
    "${file("values.yaml")}"
  ]
```

## Dependencies

The CI of this project depends on the following environment variables that should be defined in GitLab:

| Name                    | Value                                     |
|-------------------------|-------------------------------------------|
| AWS_ACCESS_KEY_ID       | access key to aws account                 |
| AWS_SECRET_ACCESS_KEY   | secret key to aws account                 |
| S3_BUCKET               | name of the bucket on aws (for tfstate)   |
| S3_REGION               | region of the bucket on aws (for tfstate) |