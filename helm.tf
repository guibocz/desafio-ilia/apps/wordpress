resource "helm_release" "wordpress-helm" {
  name       = "wordpress-helm"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "wordpress"
  version    = var.wordpress_version
  namespace  = kubernetes_namespace.wordpress-namespace.metadata.0.name

  depends_on = [
    kubernetes_namespace.wordpress-namespace,
  ]
}
