resource "kubernetes_namespace" "wordpress-namespace" {
  metadata {
    name = "wordpress-namespace"
  }

  lifecycle {
    ignore_changes = [
      metadata[0].annotations,
      metadata[0].labels,
    ]
  }
}
